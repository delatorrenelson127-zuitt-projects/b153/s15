/**
 * Create two functions:

		-First function: oddEvenChecker
			-This function will check if the number input or passed as an argument is an odd or even number.
				-Check if the argument being passed is a number or not.
					-if it is a number, check if the number is odd or even.
					-even numbers are divisible by 2.
					-log a message in the console: "The number is even." if the number passed as argument is even.
					-log a message in the console: "The number is odd." if the number passed as argument is odd.
					-if the number passed is not a number type: 
						show an alert:
						"Invalid Input."

		-Second Function: budgetChecker()
			-This function will check if the number input or passed as an argument is over or is less than the recommended budget.
				-Check if the argument being is a number or not.
					-if it is a number check if the number given is greater than 40000
						-log a message in the console: ("You are over the budget.")
					-if the number is not over 40000:
						-log a message in the console: ("You have resources left.")
					-if the argument passed is not a number:
						-show an alert: ("Invalid Input.")

		Pushing Instructions:

		Go to Gitlab:
			-in your zuitt-projects folder and access b153 folder.
			-inside your b153 folder create a new folder/subgroup: s15
			-inside s15, create a new project/repo called activity
			-untick the readme option
			
		Go to Gitbash:
			-go to your b153/s15 folder and access activity folder
			-initialize activity folder as a local repo: git init
			-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
			-add your updates to be committed: git add .
			-commit your changes to be pushed: git commit -m "includes selection control activity"
			-push your updates to your online repo: git push origin master

		Go to Boodle:
			-copy the url of the home page for your s15/activity repo (URL on browser not the URL from clone button) and link it to boodle:

			WD078-15 | Javascript - Repetition Control

 */

function oddEvenChecker(num) {
  if (typeof num === "number") {
    num % 2 === 1
      ? console.log("The number is Odd")
      : console.log("The number is Even");
  } else {
    alert("Invalid Input");
  }
}

oddEvenChecker(5); // odd
oddEvenChecker(2); // even
oddEvenChecker("text"); // invalid input

function budgetChecker(num) {
  if (typeof num === "number") {
    num < 4000
      ? console.log("You have resources left.")
      : console.log("You are over the budget.");
  } else {
    alert("Invalid Input");
  }
}

budgetChecker(3999); // "You have resources left."
budgetChecker(4001); //  "You are over the budget."
budgetChecker("invalid"); // Invalid input
