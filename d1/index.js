// Assignment Operators '='
// allows us to assign a value to a variable

let variable = "Initial variable";

/**
 * Mathematical Operator
 * addition (+)
 * subtraction (-)
 * multiplication (*)
 * division (/)
 * modulo(%)
 */

let num1 = 5;
let num2 = 10;
let num3 = 4;
let num4 = 40;

num1 = num1 + num4; // or num1 += num4;
console.log(num1); // num1 now is 45

// num1 is now 45
console.log((num1 += num1)); // num1 now is 90
console.log((num1 += 55)); // num1 now is 145

// Subtraction
console.log((num1 -= num2)); // num1 now is 135
console.log((num1 -= num4)); // num1 now is 95
console.log((num1 -= 10)); // num1 now is 95

// Multiplication
console.log((num2 *= num3)); // 40
console.log((num2 *= 5)); // 200

let string1 = "Boston";
let string2 = "Celtics";

string1 += string2;
console.log(string1); // BostonCeltics
console.log(string2); // Celtics

// console.log((num1 -= string1)); // num1 now is NaN

let mdas = 1 + 2 - (3 * 4) / 5;
// 3 * 4 = 12
// 12 / 5 = 2.4
// 1 + 2 = 3
// 3 - 2.4 = 0.6
// mdas = 0.6000000000000001
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
// 4 / 5 =.8
// 2 - 3 = -1
// -1 * .8 = -0.8
// 1 + -0.8 = 0.19999999999999996
console.log(pemdas);

// Increment and Decrement
// Increment - reassigning variable by adding 1
// Decrement - reassigning variabl by subtructing 1
let msx = 1;
++msx;
console.log(msx); //2
msx++;
console.log(msx); // 3
console.log(msx++); //3
console.log(msx); //3

/**
 * Comparison Operators
 */

console.log(1 == 1); // true

// Loose Equality ( == )
// Type coercion/forced conversion of data type in JS
let isSame = 55 == 55; // true
isSame = 55 == "55"; // true
isSame = 55 === "55"; // false
console.log(isSame);

console.log(0 == false); // true both 0 or both false
console.log(1 == true); // true both 1 or both true
console.log(true == "true"); // false,because 1 or true is not equal to "true" (string or NaN)

// Strict Equality Operator ( === )
isSame = 55 == "55"; // true, because '55' force converted to 55
isSame = 55 === "55"; // false, because different in data type

let m = 1;
let n = 1;
console.log(n == m); // true

let obj = { name: "me" };
let arr = { name: "me" };

console.log(obj == arr); // false
console.log(obj === arr); // false

console.log(obj == arr); // true
console.log(obj === arr); // true

console.log('1.5 == "1.5"', 1.5 == "1.5"); // true

// Inequality Operator (!=)
console.log('"1" != 1 is ', "1" != 1); //false

let arr1 = [1, 2];
let arr2 = [1, 2];
console.log(arr1 == arr2); // false
console.log(arr1 === arr2); // false
console.log([1, 2] == [1, 2]); // false
console.log([1, 2] === [1, 2]); // false

console.clear();

/**
 * Review: 
	Mini-Activity
		Name you variable appropriately. You may follow our guide in s14's note:

		Create a let variable with the name of your favorite food.
		Create a let variable with the sum of 150 and 9.
		Create a let variable with the product of 100 and 90.
		Create a let variable with a boolean value.
			- this variable asks if the user is active
		Create group of data with the names of you 5 favorite restaurants.
		Create a variable which describes your favorite musical artist/singer or actor/actresses with the following key:value pairs:
			firstName : <value>
			lastName : <value>
			stageName : <value>
			birthday : <value>
			age : <value>
			bestAlbum : <value>/bestTVShow : <value>
			bestSong : <value>/bestMovie : <value>
			isActive : <value>

		if the data is unavailable or there is actually NO value, add null
	Log all variables in the console.

	Create a function able to receive two numbers as arguments.
		display the quotient of the two number in the console
		return the value of the division

	Create a variable called quotient
		- this variable should be able to receive the result of division function

	Log the quotient variable's in the console with the following message:
		"The result of the division is : <valueOfQuotient>"

		- use concatenation/template literal
			`${variable}`

Take a screenshot of your console and codebased and send it in the hangouts 
 */

let myFavoriteFood = "Sinigang na Bangus";
console.log(myFavoriteFood);

let sum = 150 + 9;
console.log(sum);

let product = 100 * 90;
console.log(product);

let isActive = true;
console.log(isActive);

let favoriteResto = [
  "Kuya J",
  "Mang Inasal",
  "Vikings",
  "Gerry's Grill",
  "Jollibee",
];
console.log(favoriteResto);

let myFavoriteArtist = {
  firstname: "Aristotle",
  lastName: "Pollisco",
  stageName: "Gloc 9",
  birthday: "October 18, 197",
  age: 44,
  bestAlbum: "Diploma (2007)",
  bestSong: "Simpleng Tao",
  isActive: true,
};

console.log(myFavoriteArtist);

function division(n1, n2) {
  let m = n1 / n2;
  console.log(m);
  return m;
}

let quotient = division(4, 2);
console.log(`The result of the division is : ${quotient}`);

let x = 5000;
let y = 7000;
let w = 8000;
let numString3 = "5500";

/**
 * AND Operator if both side are the same the answer is T otherwise F
 * T && T = T
 * T && F = F
 * F && T = F
 * F && F = T
 */

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let requiredLevel = 95;
let requiredAge = 18;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1);

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2);

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); // false

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); // false

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5); // true

let userName1 = "gamer2001";
let userName2 = "shadow1991";
let userAge1 = 15;
let userAge2 = 30;

let registration1 = userName1.length > 8 && userAge1 >= requiredAge;
console.log(registration1); //false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); // false

/**
 * OR Operator if there is 1 T the result is T
 * T && T = T
 * T && F = T
 * F && T = T
 * F && F = F
 */

let userLevel1 = 100;
let userLevel2 = 65;

let guildRequirements1 =
  isRegistered && userLevel1 >= requiredLevel && userAge1 >= requiredAge;
console.log(guildRequirements1); // falses

let guildRequirements2 =
  isRegistered || userLevel1 >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirements2); // falses

let guildRequirements3 = userLevel1 >= requiredLevel || userAge1 >= requiredAge;
console.log(guildRequirements3); // true

let guildAdmin = isAdmin || userLevel2 >= requiredAge;
console.log(guildAdmin); // true

// Not Operator (!)
/**
 * turns the boolean into opposit value
 * T = F
 * F = T
 */

// Conditional statement
// if-else

function addNum(num1, num2) {
  if (typeof num1 === "number" && typeof num2 === "number") {
    console.log("Run only if both arguments passed are number types.");
    console.log(num1 + num2);
  } else {
    console.log("One or both of the arguments are not numbers.");
  }
}

addNum("1", 2);
console.clear();

function login(username, password) {
  if (typeof username === "string" && typeof password === "string") {
    console.log("Both arguments are strings.");
    // if (username.length >= 8) {
    //   alert("username is too short");
    // } else if (password.length >= 8) {
    //   alert("password is too short");
    // } else {
    //   console.log("Credentials too short.");
    // }

    if (password.length >= 8 && username.length >= 8) {
      alert("Thank you for logging in");
    } else if (username.length <= 8) {
      alert("username is too short");
    } else if (password.length <= 8) {
      alert("password is too short");
    } else {
      console.log("Credentials too short.");
    }
  }
}

let username = "user1";
let password = "user1";
// console.log(login(username, password));

/**
 * 
 Monday  - Black
 Tuesday - Green
 Wednesday - Yellow
 Thursday - Red
 Friday - Violet
 Saturday - Blue
 Sunday - White
 */
function defineColorToWear(str) {
  let msg = "Invalit input. Please use input string.";
  let colorToWear = "Invalid Input. Enter a valid day of the week.";
  let day = str;
  if (typeof str === "string") {
    if (str.toLocaleLowerCase() === "monday") {
      colorToWear = "Black";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "tuesday") {
      colorToWear = "Green";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "wednesday") {
      colorToWear = "Yellow";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "thursday") {
      colorToWear = "Red";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "friday") {
      colorToWear = "Violet";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "saturday") {
      colorToWear = "Blue";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else if (str.toLowerCase() === "sunday") {
      colorToWear = "White";
      msg = `Today is ${day.toUpperCase()}, wear ${colorToWear.toUpperCase()}`;
    } else {
      console.log(colorToWear);
    }
    console.log(msg);
  }
}

defineColorToWear("monday");
defineColorToWear("Tuesday");
defineColorToWear("wedNesday");
defineColorToWear("THURSDAY");
defineColorToWear("FRIDAY");
defineColorToWear("saturday");
defineColorToWear("sunday");
defineColorToWear("invalid");
defineColorToWear(5);

// Switch Statement
// to select one of many of code blocks/statement to be execute.
function tellWhatColorToWear(dayInput) {
  let msg = "Pleas input valid day of the week.";

  if (typeof dayInput !== "string") {
    return "Invalid input, Please input string only";
  }
  switch (dayInput.toLowerCase()) {
    case "monday":
      msg = `Today is ${dayInput}, wear black.`;
      break;
    case "tuesday":
      msg = `Today is ${dayInput}, wear green.`;
      break;
    case "wednesday":
      msg = `Today is ${dayInput}, wear yellow.`;
      break;
    case "thursday":
      msg = `Today is ${dayInput}, wear red.`;
      break;
    case "friday":
      msg = `Today is ${dayInput}, wear violet.`;
      break;
    case "saturday":
      msg = `Today is ${dayInput}, wear blue.`;
      break;
    case "sunday":
      msg = `Today is ${dayInput}, wear white.`;
      break;
    default:
      break;
  }
  return msg;
}

console.clear();
console.log(tellWhatColorToWear("Monday"));
console.log(tellWhatColorToWear("tuesday"));
console.log(tellWhatColorToWear("WEDNESDAY"));
console.log(tellWhatColorToWear("ThurSday"));
console.log(tellWhatColorToWear("friday"));
console.log(tellWhatColorToWear("satUrday"));
console.log(tellWhatColorToWear("Sunday"));
console.log(tellWhatColorToWear("invalid"));
console.log(tellWhatColorToWear(3));
